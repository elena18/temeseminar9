using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VectoriUniti
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the sizes for arrays:");
            int size1 = int.Parse(Console.ReadLine());
            int size2 = int.Parse(Console.ReadLine());
            int[] firstArray = new int[size1], secondArray = new int[size2];

            Random random = new Random();

            for (int i = 0; i < firstArray.Length; i++)
            {
                firstArray[i] = random.Next();
            }
            for (int i = 0; i < secondArray.Length; i++)
            {
                secondArray[i] = random.Next();
            }
            sort(firstArray);
            sort(secondArray);

            int[] newArray = new int[size1+size2];
            int k = 0, l = 0, size3=0;
            while(k<size1 && l<size2)
            {
                if(firstArray[k]==secondArray[l])
                {
                    newArray[size3] = firstArray[k];
                    size3++;
                    newArray[size3] = secondArray[l];
                    size3++;
                    k++; l++;
                }
                else if(firstArray[k]<secondArray[l])
                {
                    newArray[size3] = firstArray[k];
                    k++;
                    size3++;
                }
                else
                {
                    newArray[size3] = secondArray[l];
                    size3++;
                    l++;
                }
                
            }
            if (k != size1)
            {
                for(int i=k;i<firstArray.Length;i++)
                {
                    newArray[size3] = firstArray[i];
                    size3++;
                }
            }
            else if (l != size2)
            {
                for (int i = l; i < secondArray.Length; i++)
                {
                    newArray[size3] = secondArray[i];
                    size3++;
                }
            }
           
            for(int i=0;i<newArray.Length;i++)
            {
                Console.Write(newArray[i]+" ");
            }

            Console.ReadKey();
        }
        static void sort(int[] array)
        {
            for(int i=0;i<array.Length-1;i++)
            {
                for(int j=i+1;j<array.Length;j++)
                {
                    if(array[i]>array[j])
                    {
                        int aux = array[i];
                        array[i] = array[j];
                        array[j] = aux;
                    }
                }
            }
        }
    }
}
